
.. index::
   pair: Média de l'illibéralisme autoritaire macronien ; JDD
   pair: Média de l'illibéralisme autoritaire macronien ; Paris Match

.. _illiberalisme_2023_04:

=================================================================
2023-04
=================================================================


2023-04-27 **La France illibérale ? Oui, les recours à l’état d’urgence ont fait basculer le pays dans le camp autoritaire**
=====================================================================================================================================

- https://www.liberation.fr/idees-et-debats/tribunes/la-france-illiberale-oui-les-recours-a-letat-durgence-ont-fait-basculer-le-pays-dans-le-camp-autoritaire-20230427_HVOSNI2GUVFIVPI42FOZJJVO5M/

Restrictions de la liberté de manifester, proximité de la justice avec
l’exécutif, rhétorique de «l’ennemi intérieur» et multiples rappels à
l’ordre par l’Europe ou l’ONU : les grands principes définissant les
démocraties illibérales peuvent bel et bien s’appliquer au modèle français,
analyse la juriste Eugénie Mérieau.

Des violences policières aux attaques contre les associations, en passant
par l’usage répété du 49.3, la France a-t-elle glissé du côté des démocraties
illibérales ?

Popularisé dans les années 2010, l’illibéralisme désigne un ­régime hybride
combinant une légitimité tirée du scrutin démocratique à une concentration
du pouvoir et la gestion répressive des libertés.

Si pour la constitutionnaliste la patrie des droits de l'homme a glissé vers
un autoritarisme de type hongrois ou israelien, l'historien Marc Lazar rappelle
...


2023-04-18 **TOURNÉE GÉNÉRALE #5 AVEC GUILLAUME MEURICE**
==============================================================

On va rire, chanter, écouter des gens qui savent des trucs, ou qui ont
des solutions.

Par exemple, il y aura LES FATALS PICARDS, Audrey Vernon, Arnaud Maillard,
l'archéologue, Rozenn Colleter, Patrick Baudouin président de la Ligue
des droits de l'Homme

Et en invitée politique : Clémentine Autain avec Paloma Moritz.

- 5:58 : Les Fatals picards, https://www.youtube.com/watch?v=3BTmrzbErqs&t=358s
- 18:01 : Audrey Vernon, comédienne, https://www.youtube.com/watch?v=3BTmrzbErqs&t=1081s
- 30:52 : Miguel Izaga de la Coordinadora Memoria Contra la Impunidad, https://www.youtube.com/watch?v=3BTmrzbErqs&t=1852s
- 42:00 : Rozenn Colleter, anthropologue, https://www.youtube.com/watch?v=3BTmrzbErqs&t=2520s
- 55:55 : Retour des Fatals picards, https://www.youtube.com/watch?v=3BTmrzbErqs&t=3355s
- 1:09:58 : Patrick Baudouin, président de la Ligue des droits de l'Homme, https://www.youtube.com/watch?v=3BTmrzbErqs&t=4198s
- 1:26:46 : Arnaud Maillard, comédien, https://www.youtube.com/watch?v=3BTmrzbErqs&t=5206s
- 1:36:49 : Clémentine Autain, députée LFI, invitée politique de Paloma Moritz, https://www.youtube.com/watch?v=3BTmrzbErqs&t=5809s


2023-04-18 **Désormais, Emmanuel Macron est rejeté par (toute) la pensée**
==========================================================================================

- https://www.mediapart.fr/journal/culture-et-idees/180423/emmanuel-macron-rejete-par-toute-la-pensee

Désormais, Emmanuel Macron est rejeté par (toute) la pensée.

Il est désavoué aussi bien par des penseurs de renom et des universitaires
modérés que par des représentants de la pensée « mainstream », voire « unique ».

Les « terroristes intellectuels » visés par le ministre de l’intérieur
déclenchent leur arsenal verbal non pas depuis les bastions intellectuels
habituels de la gauche, mais depuis le Collège de France.

Invité de l’émission « Quotidien », lundi 17 février, l’historien
Pierre Rosanvallon, titulaire de la chaire d’histoire moderne et
contemporaine du politique de 2001 à 2018, a considéré que « nous sommes
en train de traverser, depuis la fin du conflit algérien, la crise
démocratique la plus grave que la France ait connue ».

Reconnaissant que la lettre des institutions et des lois a été respectée,
ce spécialiste de la démocratie juge néanmoins que « l’esprit est bafoué ».



2023-04-14 **Du ministre de l'Intérieur à la Première ministre : des menaces à peine voilées contre la LDH** (**glissement vers l'illibéralisme**)
====================================================================================================================================================

- https://www.change.org/p/stop-%C3%A0-l-escalade-r%C3%A9pressive/u/31495245


Chers tous, chères toutes,

Encore une fois, la LDH vous remercie de votre soutien.

L’actualité nous oblige plus que jamais à porter le rôle de contre-pouvoir
de la LDH, défendant les droits et les libertés de toutes et tous, notamment
contre les violences policières.

Dans ce cadre, nous avons fait l’objet de menaces à peine voilées de la
part du ministre de l’Intérieur, puis de la Première ministre.

Alors que nous attendions que les propos de M. Darmanin soient recadrés
dans un cadre plus républicain et plus respectueux de la liberté associative,
nous avons au contraire fait l’objet de critiques fallacieuses de la part
d’Elisabeth Borne, instillant le doute sur les missions de la LDH.

Ses propos sont particulièrement graves en ce qu’elle est la première
ministre du pays et, qu’hormis sous l’Occupation, nous n’avons jamais
été attaqués aussi frontalement par un gouvernement.

Deux choses ont servi de base aux attaques de la Première ministre contre
la LDH : le dépôt d’un référé-liberté pour contester un arrêté de la
préfète des Deux-Sèvres interdisant le transport d’armes par destination
à Sainte-Soline ainsi que son prétendu « islamisme radical ».

Les contre-vérités absolues énoncées sur ces deux sujets ont été démenties
point par point par la LDH afin de mettre un terme à la déformation de
ses positions.

En dépit de ces critiques, nous continuerons à lutter avec détermination
pour protéger les valeurs que nous avons défendu de tout temps : la liberté,
l’égalité, la dignité de la personne, la fraternité et ce, **face à un pouvoir
qui glisse progressivement vers l’illibéralisme et rogne les libertés publiques,
d’état d’urgence en état d’urgence**.

Plus que jamais, nous avons besoin de vous pour amplifier notre voix.
Plus que jamais, nous continuerons à dénoncer les atteintes aux droits
et libertés.

Alors signez et partagez autour de vous, tant que vous le pouvez, cette pétition.

N'hésitez pas à nous soutenir : https://soutenir.ldh-france.org/



2023-04-11 Media illiberaux : Le Monde "L'Officiel du Spectacle et son obsession d'être au chevet du macronisme crevard, plutôt que de la démocratie et de la société"
========================================================================================================================================================================

- https://piaille.fr/@Pr_Logos/110180779283710556
- https://www.lemonde.fr/politique/article/2023/04/11/executif-et-majorite-cherchent-une-mesure-fiscale-pour-repondre-a-la-crise-sociale_6169058_823448.html

L'Officiel du Spectacle et son obsession d'être au chevet du macronisme
crevard, plutôt que de la démocratie et de la société.


- https://piaille.fr/@Pr_Logos/110180781917896951

Toujours pas un mot sur la déchéance macronnarde dans l'illibéralisme
autoritaire, sous la houlette d'un ministre d'extrême-droite, corrompu,
antisémite, (ad nauseam).


2023-04-08 "Président de la violence et de l'hypocrisie" "La convention sur le climat n'est pas respectée"
==============================================================================================================

- https://nitter.unixfox.eu/BonPote/status/1645843536799989769#m

"Président de la violence et de l'hypocrisie"
"La convention sur le climat n'est pas respectée"

Emmanuel Macron a été interrompu à la Haye par des manifestants.
Les militants alertent sur les violences policières en France et son
inaction climatique.



2023-04-08 **On ne dissout pas un mouvement, on ne dissout pas une révolte, on ne dissout ni l’espoir, ni le courage. On ne dissout pas une idée dont l’heure est venue**
=============================================================================================================================================================================

- https://blogs.mediapart.fr/partager-cest-sympa/blog/080423/45-personnalites-contre-la-dissolution-des-soulevements-de-la-terre
- https://www.youtube.com/watch?v=yS7GKZaMKAk


C'est historique. 45 personnalités répondent dans cette vidéo à la tentative
de dissolution des Soulèvements de la Terre, prévue pour mercredi 12 avril 2023
en Conseil des Ministres. Mais comme ils et elles le disent :

::

    « On ne dissout pas un mouvement, on ne dissout pas une révolte, on ne
    dissout ni l’espoir, ni le courage.
    On ne dissout pas une idée dont l’heure est venue. »


.. _monde_illiberale_2023_04_06:

2023-04-07 La une abjecte du Monde en ligne hier soir (2023-04-06) pas le plus petit mot sur la dérive autoritaire et illibérale pointée dans tous les journaux étrangers
==============================================================================================================================================================================

- https://piaille.fr/@Pr_Logos/110155938918443655

1/ La une abjecte du Monde en ligne hier soir, avec ce pseudo-journalisme
réduit à la bureaucratie de fichier Excel, sans le plus petit mot sur la
dérive autoritaire et illibérale pointée dans tous les journaux étrangers.

.. figure:: images/monde_une_abjecte_2023_04_06.png


.. _media_illiberaux_2023_04_07:

2023-04-07 Bolorrhée a le contrôle de deux médias néofascistes (Europe 1 et CN*ws) et de deux médias de l'illibéralisme autoritaire macronien (JDD et Paris Match)
=========================================================================================================================================================================

- https://piaille.fr/@Pr_Logos/110155991312689969

La désinformation trumpo-bolsonariste devient hégémonique.

- https://www.radiofrance.fr/franceinter/dans-l-attente-d-un-feu-vert-de-bruxelles-l-ombre-de-bollore-plane-sur-paris-match-8912361


.. _controle_social_2023_04_06:

2023-04-06 Le contrôle social à la Chinoise, liberticide, immonde qui sera déployé à l'occasion de ces jeux du cirque abjects… persistera
=============================================================================================================================================


- https://piaille.fr/@Pr_Logos/110153848907745702


Le contrôle social à la Chinoise, liberticide, immonde qui sera déployé à l'occasion de ces jeux du cirque abjects… persistera.

Coups de cliquets de la dérive autoritaire et illibérale.

- https://www.lemonde.fr/societe/article/2023/04/06/la-videosurveillance-intelligente-sera-finalement-experimentee-jusqu-a-six-mois-apres-les-jeux-olympiques_6168540_3224.html


.. _black_rock_2023_04_06:

2023-04-06 🇺🇸 **Au moins rend-il compte de l'occupation de Blackrock, et comprend-il le symbole de la Rotonde**
=====================================================================================================================

- https://piaille.fr/@Pr_Logos/110156046385268321

Même le quotidien conservateur New-York Post parvient à dire des choses
de la journée d'hier moins inepte que Le Monde.

Au moins rend-il compte de l'occupation de Blackrock, et comprend-il le
symbole de la Rotonde.

- https://nypost.com/2023/04/06/french-protesters-occupy-blackrock-firm-torch-restaurant/


2023-04-06 Macronisme : du libéralisme autoritaire à la disruption sadique
=================================================================================

- https://blogs.mediapart.fr/dominique-g-boullier/blog/060423/macronisme-du-liberalisme-autoritaire-la-disruption-sadique

L’obstination et le mépris méthodique avec laquelle E. Macron a conduit
toutes ses initiatives (« son projet ») contre vents et marées constitue
une version exacerbée de l’autoritarisme spontané du libéralisme.

La disruption censée libérer la créativité s’y transforme en injonction
sadique à s’adapter.


L’écart est si grand entre l’image du candidat de 2017 et sa pratique du
pouvoir aujourd’hui encalminé dans sa contre-réforme des retraites qu’on
peut se demander comment de telles tendances peuvent se conjuguer.

Si je mobilise ici des concepts qui peuvent paraitre relever de la clinique
psychologique des individus, je tente pourtant de faire le portrait d’une
époque, celui d’une dérive systémique du libéralisme financier, de l’injonction à
l’innovation et de l’usage de tous les ressorts institutionnels
autoritaires de la Vème République au service de ces objectifs.

...

Sans doute, nommer le mal qui nous saisit aide à mieux s’en protéger.

Cependant, les solutions ne sont pas si évidentes et le cycle actuel
toujours plus répressif peut encore durer.

Ceux qui ont soutenu Macron à ses débuts peuvent cependant être affectés
par cette dérive qui peut se retourner contre eux. Sans doute sont-ils
les seuls à pouvoir débrancher une telle mécanique.

La pression du mouvement social sera donc particulièrement nécessaire
paradoxalement pour faire fuir les alliés et renforcer encore l’isolement
présidentiel.



2023-04-06  **Darmanin : la terreur**
=====================================


- https://piped.video/watch?v=IIKh4q0OIvA

Ce qui inquièterait G. Darmanin, ce serait le «terrorisme intellectuel»
de l'«Ultragauche».

Des propos lourds de très graves menaces pour la démocratie.

Nouvel épisode de Quelle époque formidable, la chronique de @vivelefeu


Dans le long entretien qu’il a accordé au Journal du dimanche, le ministre
de l’Intérieur tient en effet des propos extrêmement inquiétants.

Lorsqu’il est interrogé sur les violences policières dont les images ont
fait le tour du monde, il répond par exemple, après avoir déjà menti en
soutenant que les gendarmes n’avaient pas utilisé d’armes de guerre à
Sainte-Soline, qu’il - je cite - « refuse de céder au terrorisme intellectuel
de l’extrême gauche qui consiste à renverser les valeurs » en faisant
passer « les casseurs » pour des « agressés », et  « les policiers »
pour des « agresseurs ».

Plus loin, il dénonce – je cite encore – des « fake news » et des
« manipulations de l’information ».

Et plus loin encore, il déclare très sérieusement que la NUPES - au sein
de laquelle se sont réunis les écologistes, La France insoumise,
le Parti communiste et le Parti socialiste – est, je cite toujours,
« un mouvement qui prend la pente de l’ultragauche des années 1970 »,
c’est-à-dire d’une ultragauche qui pratiquait éventuellement la lutte armée.

Tranquillement, Darmanin passe donc de la gauche à l’extrême gauche, puis
de l’extrême gauche à l’ultragauche, puis de l’ultragauche à l’ultragauche
pratiquant la lutte armée – pour nous suggérer que tout ça ne forme
finalement qu’un seul et même ensemble de quasi-terroristes.

Et il faut s’attarder sur ces hallucinantes déclarations, car bien sûr :
on pourrait se contenter d’en rire, en imaginant Olivier Faure en train
de s’acheter un pistolet automatique pour attaquer la Banque de France.

Mais en réalité, les propos du ministre de l’Intérieur ne sont pas du tout
rigolos : ils sont au contraire lourds de très graves menaces pour la démocratie »

Nouvel épisode de Quelle époque formidable, la chronique politique de
Sebastien Fontenelle, à retrouver tous les jeudis sur Blast !
