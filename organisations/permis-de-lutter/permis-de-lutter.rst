.. index::
   pair: Sites; Permis de lutter
   ! Permis de lutter

.. _permis_de_lutter:

=================================================================
**Permis de lutter** (de Politis)
=================================================================

- https://www.politis.fr/permis-de-lutter/
- https://politisvigie-permisdelutter.my.canva.site/permis-de-lutter
- https://canada.unofficialbird.com/permisdelutter/rss


.. figure:: images/permis_de_lutter.jpeg
   :align: center


Introduction
==============

Depuis des années, nous observons, inquiets, le glissement autoritaire
du pouvoir exécutif.

Interdictions de manifester, menaces de dissolution d’associations,
attaques contre la Ligue des Droits de l’Homme, violences policières.

Jusqu’aux arrêtés préfectoraux interdisant les ustensiles de cuisine,
devenus armes par destination !

Les récits de citoyen·es brutalisé·es, les images de visages abîmés et
de corps mutilés se multiplient.

A Politis, nous suivons ces évolutions de près.

Que dire d’un pouvoir qui cherche à asphyxier la contestation politique ?
Qui veut réduire au silence des mouvements sociaux, des luttes écologiques,
des citoyens simplement engagés pour la défense de leurs droits ou pour
que notre planète soit vivable dans les prochaines décennies ?

La banalisation de ces violences devenues “ordinaires” nous interroge
sur l’état de notre démocratie.

Pour renforcer notre travail d’enquête et d’analyse, nous lançons le
projet “Permis de Lutter”, qui proposera :

- des enquêtes sur la mécanique de la répression et ses différentes
  formes – policière, judiciaire, administrative, physique ou psychologique.
- un suivi régulier, via notre compte twitter @Permisdelutter, des évènements
  qui illustrent ces nouvelles formes de criminalisation des luttes sociales et écologiques,
- une boîte à outils, pour comprendre ce qui se joue, et vous informer sur vos droits.


.. _pourquoi_permis_de_lutter:

Pourquoi Politis lance ce projet ?
========================================

**Un projet journalistique essentiel, pour informer sur l’état de notre
démocratie.**

Pour que les citoyens puissent continuer d’user de leur droit de manifester,
agir, protester, s’engager pour la justice sociale, l’égalité des droits
ou l’avenir de notre humanité, sans risquer à chaque instant de se faire
enfermer, intimider, frapper, gazer ou humilier.

Aucune entrave à l’Etat de droit ne doit être passée sous silence. "

Agnès Rousseaux, directrice de Politis

" La mécanique de la violence – matraques, insultes, intimidations,
humiliations, arrestations et détentions arbitraires – s’installe dans
l’espace public et s’intensifie jusqu’à ce que les gueules abîmées, les
bras cassés, les yeux au beurre noir ou les traumatismes crâniens deviennent
les images normales, banales, des mobilisations.

Dans quelle démocratie  intimide-t-on, réprime-t-on, enferme-t-on ses
adversaires politiques ? "

Pierre Jacquemain, rédacteur en chef de Politis


Sur quoi allons-nous enquêter ?
===================================

.. _techniques_maintien_de_lordre:

Les techniques et stratégies de maintien de l’ordre
-------------------------------------------------------

Pour jouer notre rôle de vigie, nous voulons identifier toutes les armes
et techniques utilisées par les forces de l’ordre pour réprimer les
mouvements sociaux et écologiques.

Moyens de surveillance, utilisation d’armes de guerre, types de brigades,
ordres et stratégies de terrain, nouvelles technologies, comme les drones.

Vous connaissez les produits marquants codés, ces additifs incorporés
dans l’eau des canons à eau et gaz lacrymogène et réactif aux UV pendant
plusieurs mois sur les vêtements et plusieurs semaines sur la peau ?
**Ils permettent de repérer, lors d’un contrôle de police, les personnes
engagées. Bienvenue à Gattaca !**


.. _evolution_procedures_judiciaires:

L'évolution des procédures judiciaires
------------------------------------------

Nous suivrons au plus près les procès de chaque personne poursuivie pour
des actions ou une participation à des manifestations, pour analyser la
réaction judiciaire sur le temps long.

Et comprendre l’évolution des jurisprudences et les dévoiements de
procédures : utilisation de la garde à vue comme entrave au droit de
manifester, prise d’empreintes et d’ADN à des fins de fichages même
lorsque le dossier est classé sans suite, assignations à résidence,
perquisitions abusives, interpellations préventives,...

Une inventivité sans limite !


Les décisions administratives abusives
-----------------------------------------

La répression contre les mouvements sociaux s’organise aussi au niveau
administratif.

Par exemple au travers de décision d’interdiction de manifestations, de
manière abusive.

Mais aussi par l'interdiction de certaines subventions par le préfet, les
dissolutions, les recours devant les tribunaux et le Conseil d’état.


Le suivi du travail législatif
------------------------------------

Ces méthodes de répression sont permises par une multiplication des lois
portant atteinte aux libertés individuelles et collectives.

Etat d’urgence, Loi sécurité globale, Loi anti casseur, Loi dite « séparatisme »
qui s’applique désormais contre le monde associatif,...

Le gouvernement prépare des lois Immigration ou encore la Loi JO2024 qui
élargit les possibilités de surveillance et inquiète par exemple les
associations d’aide aux migrants.
