.. index::
   pair: Sites; Permis de lutter
   ! Flagrant déni

.. _flagrant_deni:

=================================================================
**Flagrant déni** (dévoiler les rouages de l'impunité policière)
=================================================================

- http://www.flagrant-deni.fr/
- https://www.ici-grenoble.org/article/appel-a-dons-pour-le-17
- https://www.flagrant-deni.fr/feed/
- https://linktr.ee/flagrantdeni
- https://www.helloasso.com/associations/flagrantdeni/collectes/financez-le-17-de-flagrant-deni-guide-juridique-d-aide-aux-victimes
- https://www.flagrant-deni.fr/site-amis/
- https://www.flagrant-deni.fr/presse-paresseuse/

::

    contact@flagrant-deni.fr

**Flagrant déni : un média engagé dans la lutte contre l’impunité policière**
===============================================================================

Nous publions des informations situées mais toujours vérifiées.

Des enquêtes, pour montrer que les violences policières résultent plus
de pratiques institutionnelles que de bavures personnelles.

Des contre-enquêtes, pour décrypter les mécanismes judiciaires qui fabriquent
l’impunité et donner des outils aux victimes.

Des témoignages, pour aider les victimes à dépasser l’isolement que génère
la violence et créer un récit collectif, sensible, et politique.

Des images, pour s’éloigner des représentations grises et obsédantes qui
dominent le sujet.

Flagrant déni est porté par une association loi 1901, dotée d’un statut
de média. Elle est animée par une équipe de salarié·es et de bénévoles.
